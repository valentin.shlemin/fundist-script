// firstPayments.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test

// <reference types="cypress" />

import * as data from '../creds.json';

const casinoName = data.casinoName;

describe('Fundist login', () => {
    it('gotoFundistSites', () => {
      cy.visit('https://test.fundist.org/ru/Stalls')
      cy.wait(1000)
      cy.get('#Login')
        .type('vshlemin')
        .should('have.value', 'vshlemin')
      cy.get('#Password')
        .type('Test123!')
        .should('have.value', 'Test123!')
      cy.get('#LoginBtn').contains('Войти').click()
      cy.wait(2000)

      cy.get('#AddStallButton').contains('Добавить сайт').click({ force: true })
      cy.wait(2000)
      
      cy.get('.modal-dialog').within(() => {
        cy.get('.select2-selection__rendered').contains('Сеть').click()
      })

      cy.get('.select2-dropdown').within(() => {
        cy.get('.select2-search__field')
          .type(casinoName)
          .should('have.value', casinoName)
          .type ('{enter}')
      })

      cy.get('.modal-dialog').within(() => {
        cy.get('[name=Name]').click()
          .type(casinoName)
      })

      cy.get('[name="BalanceAlert"]')
          .type(1000)
          .should('have.value', 1000)

      cy.get('.modal-dialog').within(() => {
        cy.get('.select2-selection__rendered').contains('Выберите тип казино').click()
      })
      cy.get('.select2-results__option').contains('WLC').click()

      cy.get('#curr1').check()
      cy.get('#curr2').check()
      cy.get('#curr0').check() //test curr0 qa curr3

      cy.get('#langen').check()
      cy.get('#langru').check()
      
      cy.get('#NewStallBlockSaveButton').contains('Сохранить').click()

    })
  })
