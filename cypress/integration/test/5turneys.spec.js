// firstPayments.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test

// <reference types="cypress" />

import * as data from '../creds.json';

const casinoName = data.casinoName;

describe('Fundist login', () => {
    it('gotoFundistPayments', () => {
      cy.visit('https://test.fundist.org/ru/Loyalty/Tournaments')
      cy.wait(1000)
      cy.get('#Login')
        .type('vshlemin')
        .should('have.value', 'vshlemin')
      cy.get('#Password')
        .type('Test123!')
        .should('have.value', 'Test123!')
      cy.get('#LoginBtn').contains('Войти').click()
      cy.wait(2000)

      // NEW TOURNAMENT

      cy.get('.loyalty-modificators-api').within(() => {
        cy.get('.select2-selection__rendered').click()
      })
      cy.get('.select2-dropdown').within(() => {
        cy.get('.select2-search__field')
          .type(data.casinoName)
          .should('have.value', data.casinoName)
          .type ('{enter}')
      })
      cy.wait(2000)
      cy.get('#NewTournamentButton').click({ force: true })
      cy.get('#MainName')
          .type('Tornament №1 daily')
          .should('have.value', 'Tornament №1 daily')
          .tab()
          .type('Турнир №1 ежедневный')
          .should('have.value', 'Турнир №1 ежедневный')
      cy.get('[error-text="Введите размер взноса"]')
          .type('100')
          .should('have.value', '100')
      cy.get('.input-group-addon').within(() => {
          cy.get('.fa-calendar').click()
      })
      cy.get('#PeriodDays').select('1 day')
      cy.get('#Tournaments_Repeats').select('1 day')
      cy.get('#Result-Tab').click()
      cy.get('#result_value')
          .type('100')
          .should('have.value', '100')
          .tab()
          .wait(100)
          .tab()
          .type('50')
          .should('have.value', '50')
          .tab()
          .tab()
          .type('30')
          .should('have.value', '30')
          .tab()
          .tab()
          .type('10')
          .should('have.value', '10')
          .tab()
          .tab()
          .type('5')
          .should('have.value', '5')
          .tab()
          .tab()
          .type('3')
          .should('have.value', '3')
          .tab()
          .tab()
          .type('2')
          .should('have.value', '2')
      cy.get('#SaveTournamentButton').click()
      cy.wait(2000)
      // cy.get('#SaveModalDangerousValuesConfirm').click()
      // cy.wait(2000)
      cy.get('#SaveModalSuccessConfirm').click()
      cy.wait(2000)

      // COPY WEEKLY

      cy.get('#CopyCurrentTournamentButton').click()
      cy.wait(1000)
      cy.get('#CopyName')
        .clear()
        .type('Tornament №1 weekly')
        .should('have.value', 'Tornament №1 weekly')
      cy.get('#CopyTournamentCloseButton').click()
      cy.wait(4000)
      cy.get('#SaveModalSuccessConfirm').click()
      cy.wait(4000)
      cy.get('#MainPreferences-Tab').click()
      cy.get('#MainName')
        .clear()
        .type('Tornament №1 weekly')
        .should('have.value', 'Tornament №1 weekly')
        .tab()
        .clear()
        .type('Турнир №1 еженедельный')
        .should('have.value', 'Турнир №1 еженедельный')
      cy.get('#PeriodDays').select('1 week')
      cy.get('#Tournaments_Repeats').select('1 week')
      cy.get('#SaveTournamentButton').click()
      cy.wait(4000)
      cy.get('#SaveModalSuccessConfirm').click()
      cy.wait(2000)

      // COPY MOUTHLY

      cy.get('#CopyCurrentTournamentButton').click()
      cy.wait(2000)
      cy.get('#CopyName')
        .clear()
        .type('Tornament №1 mouthly')
        .should('have.value', 'Tornament №1 mouthly')
      cy.get('#CopyTournamentCloseButton').click()
      cy.wait(4000)
      cy.get('#SaveModalSuccessConfirm').click()
      cy.wait(4000)
      cy.get('#MainName')
        .clear()
        .type('Tornament №1 mouthly')
        .should('have.value', 'Tornament №1 mouthly')
        .tab()
        .clear()
        .type('Турнир №1 ежемесячный')
        .should('have.value', 'Турнир №1 ежемесячный')
      cy.get('#PeriodDays').select('1 month')
      cy.get('#Tournaments_Repeats').select('1 month')

      cy.get('#SaveTournamentButton').click()
      cy.wait(4000)
      cy.get('#SaveModalSuccessConfirm').click()

    })
  })
