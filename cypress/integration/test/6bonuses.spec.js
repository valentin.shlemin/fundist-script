// firstPayments.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test

// <reference types="cypress" />

import * as data from '../creds.json';

const casinoName = data.casinoName;

describe('Fundist login', () => {
    it('gotoFundistPayments', () => {
      cy.visit('https://test.fundist.org/ru/Loyalty/Bonuses')
      cy.wait(1000)
      cy.get('#Login')
        .type('vshlemin')
        .should('have.value', 'vshlemin')
      cy.get('#Password')
        .type('Test123!')
        .should('have.value', 'Test123!')
      cy.get('#LoginBtn').contains('Войти').click()
      cy.wait(2000)

      cy.get('#DivApiList').within(() => {
        cy.get('.select2-selection__rendered').click()
      })
      cy.get('.select2-container--open').within(() => {
        cy.get('.select2-search__field').type('draft').type('{enter}')
      })
      cy.wait(2000)
      cy.get('.bonuses-header').within(() => {
        cy.get('a').contains('Бонусы').click()
      })
      cy.wait(2000)
      cy.get('a').contains('404572').click()
      cy.wait(2000)
      cy.get('#copyBonusButton').click()
      cy.wait(1000)
      cy.get('#confirmCopyBonus').click()
      cy.wait(2000)
      cy.get('.modal-dialog').within(() => {
        cy.get('#select2-ApiListCopy-container').click()
      })
      cy.get('.select2-container--open').within(() => {
        cy.get('#select2-CopyApi-input').type(casinoName).type('{enter}')
      })
      cy.get('#CopyBonusCloseButton').click()
      cy.get('#SaveModalSuccessConfirm').click()




      // cy.get('#select_bonuses_id').within(() => {
      //   cy.get('span').contains('404572').click()
      // })
      // cy.get('.select2-results__options--nested').within(() => {
      //   cy.get('li').contains('404560').click()
      // })

    })
  })
