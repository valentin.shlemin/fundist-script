// firstPayments.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test

// <reference types="cypress" />

import * as data from '../creds.json';

const casinoName = data.casinoName;
const casinoType = data.casinoType;
const wlcSecret = data.testWlcSecret; 

const ipAdress = "10.70.3.1,\n10.70.3.33,\n10.70.3.55,\n213.21.217.252,\n213.21.217.251,\n10.70.3.3,\n89.111.33.17,\n87.99.94.34,\n188.130.240.79,\n89.111.33.131,\n192.168.88.72,\n79.120.104.6,\n109.196.164.211,\n178.176.196.39,\n172.20.0.1,\n10.110.0.12,\n89.111.53.68,\n10.110.40.11,\n78.81.245.219,\n109.229.76.101,\n192.168.77.2,\n89.111.53.69"; 
const paymentSuccess = "https://test-"+casinoName.toLowerCase()+".egamings.com?message=PAYMENT_SUCCESS";
const paymentFail = "https://test-"+casinoName.toLowerCase()+".egamings.com?message=PAYMENT_FAIL";
const wlcProxy = casinoType.toLowerCase()+casinoName.toLowerCase()+"_test";

//ne vsegda .com
const ruLegal = casinoName+".com is operated by SG International N.V., which is registered in Heelsumstraat 51 E-Commerce Park, Curacao, registration No. 137028 and having a gaming licence No.8048/JAZZ2015-035 and all rights to operate the gaming software. SG International LP, with its registered address Suite 1 4 Queen Street, Edinburgh, United Kingdom, EH2 1JE, registration No. SL023302, is providing payment services as an agent according to the licence agreement concluded between SG International LP and SG International N.V."
const enLegal = casinoName+".com является зарегистрированной торговой маркой SG International N.V. по адресу Хельзумштрат 51, Кюрасао и регистрационным номером 137028. Игорная деятельность организована в соответствии с правилами игорной лицензии. Номер игорной лицензии No.8048/JAZZ2015-035. Все права на эксплуатацию программного обеспечения защищены. В соответствии с этим лицензионным соглашением, посредником (исполнителем) в предоставлении платежных услуг для SG International N.V. (заказчика) выступает SG International LP, Сьют 1, Улица Квинз 4,Эдинбург, EH21JE, Шотландия, Великобритания, рег.номер SL023302. Заказчик SG International N.V. принимает эти условия"

if (wlcSecret.length === 64) {
  console.log('ok')
}

describe('Fundist login', () => {
    it('gotoFundistSites', () => {
      cy.visit('https://test.fundist.org/ru/Api')
      cy.wait(1000)
      cy.get('#Login')
        .type('vshlemin')
        .should('have.value', 'vshlemin')
      cy.get('#Password')
        .type('Test123!')
        .should('have.value', 'Test123!')
      cy.get('#LoginBtn').contains('Войти').click()
      // cy.wait(10000)

      cy.get('#buttonAddNewApi').contains('Добавить доступ').click({ force: true })
      cy.wait(1000)
      cy.get('.modal-dialog').within(() => {
        cy.get('.select2-selection__rendered').contains('Сеть').click()
      })
      cy.get('.select2-results__option').contains(casinoName).click()
      cy.wait(1000)
      cy.get('.modal-dialog').within(() => {
        cy.get('.select2-selection__rendered').contains('Сайт').click()
      })
      cy.get('.select2-results__option').contains(casinoName).click()
      cy.get('.modal-dialog').within(() => {
        cy.get('[name="Name"]').click()
          .type(casinoName)
      })
      cy.get('.modal-dialog').within(() => {
        cy.get('.select2-selection__rendered').contains('Тип').click()
      })
      cy.get('.select2-results__option').contains('WLC').click()
      cy.get('.modal-dialog').within(() => {
        cy.get('[name="IP"]').type(ipAdress, { delay: 1 })
      })
      cy.get('#Loyalty').check()
      cy.get('#Tournaments').check()
      cy.get('#MobileGames').check()
      cy.get('.modal-dialog').within(() => {
        cy.get('[name="UrlPaymentSuccess"]').type(paymentSuccess, { delay: 1 })
      })
      cy.get('.modal-dialog').within(() => {
        cy.get('[name="UrlPaymentFail"]').type(paymentFail, { delay: 1 })
      })
      cy.get('.modal-dialog').within(() => {
        cy.get('[name="WlcProxyPath"]').type(wlcProxy, { delay: 1 })
      })
      cy.get('.modal-dialog').within(() => {
        cy.get('[name="WlcSecret"]').type(wlcSecret)
      })
      cy.get('#AllowZeroIPHash').check() 
      cy.get('#ModalApiSaveButton').contains('Сохранить').click()
      cy.get('#MessageWindowCloseButton').contains('Закрыть').click()
    })
  })
