// firstPayments.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test

// <reference types="cypress" />

import * as data from '../creds.json';

const casinoName = data.casinoName;

const paymentObj = [
      {"name":"AstroPay Card", "regular":/^AstroPay Card$/},
      {"name":"AstroPay Streamline", "regular":/^AstroPay Streamline$/},
      {"name":"Inpay Withdraw", "regular":/^Inpay Withdraw$/},
      {"name":"Neteller", "regular":/^Neteller$/},
      {"name":"Webmoney", "regular":/^Webmoney$/},
      {"name":"PayCryptos Bitcoin", "regular":/^PayCryptos Bitcoin$/},
      {"name":"Paypal", "regular":/^Paypal$/},
      {"name":"Skrill", "regular":/^Skrill$/},
      {"name":"Wirecard", "regular":/^Wirecard$/},
];

describe('Fundist login', () => {
    it('gotoFundistPayments', () => {
      cy.visit('https://qa.fundist.org/ru/Payments/Config')
      cy.wait(1000)
      cy.get('#Login')
        .type('vshlemin')
        .should('have.value', 'vshlemin')
      cy.get('#Password')
        .type('Test123!')
        .should('have.value', 'Test123!')
      cy.get('#LoginBtn').contains('Войти').click()
      //cy.wait(5000)

      cy.get('[title=Api]').contains('Api').click({ force: true })
      
      cy.get('.select2-container--open').within(() => {
        cy.get('.select2-search__field')
          .type(casinoName)
          .should('have.value', casinoName)
          .type ('{enter}')
      })
      //payment loop
      cy.wrap(paymentObj).each((index) => {
        cy.get('#ButtonFilter').click({ force: true })
        cy.wait(2000)
        cy.get('#buttonAddConfig').click({ force: true })
        cy.wait(2000)
        cy.get('.modal-dialog').within(() => {
          cy.get('.select2-selection__rendered').contains('Платёжная система').click()
        })
        cy.get('.select2-search--dropdown').within(() => {
          cy.get('.select2-search__field')
            .type(index.name)
            .should('have.value', index.name)
        })
        cy.get('.select2-results__option').contains(index.regular).click()
        cy.get('#IDCurrency1').check()
        cy.get('#IDCurrency2').check()
        cy.get('#IDCurrency3').check() //test IDCurrency0
        cy.get('#ConfigModalSubmitButton').contains('Сохранить').click()
      }) 
    })
  })
