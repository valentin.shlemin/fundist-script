// firstPayments.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test

// <reference types="cypress" />

import * as data from '../creds.json';

const casinoName = data.casinoName;
const casinoType = data.casinoType;
const wlcSecret = data.qaWlcSecret;
const wlcURL = data.casinoUrl;

const fCountry = 'TestPasha';

const enLegal = wlcURL + " is operated by SG International N.V., which is registered in Heelsumstraat 51 E-Commerce Park, Curacao, registration No. 137028 and having a gaming licence No.8048/JAZZ2015-035 and all rights to operate the gaming software. SG International LP, with its registered address Suite 1 4 Queen Street, Edinburgh, United Kingdom, EH2 1JE, registration No. SL023302, is providing payment services as an agent according to the licence agreement concluded between SG International LP and SG International N.V."
const ruLegal = wlcURL + " является зарегистрированной торговой маркой SG International N.V. по адресу Хельзумштрат 51, Кюрасао и регистрационным номером 137028. Игорная деятельность организована в соответствии с правилами игорной лицензии. Номер игорной лицензии No.8048/JAZZ2015-035. Все права на эксплуатацию программного обеспечения защищены. В соответствии с этим лицензионным соглашением, посредником (исполнителем) в предоставлении платежных услуг для SG International N.V. (заказчика) выступает SG International LP, Сьют 1, Улица Квинз 4,Эдинбург, EH21JE, Шотландия, Великобритания, рег.номер SL023302. Заказчик SG International N.V. принимает эти условия"

const ipAdress = "10.70.3.1,\n10.70.3.33,\n10.70.3.55,\n213.21.217.252,\n213.21.217.251,\n10.70.3.3,\n89.111.33.17,\n87.99.94.34,\n188.130.240.79,\n89.111.33.131,\n192.168.88.72,\n79.120.104.6,\n109.196.164.211,\n178.176.196.39,\n172.20.0.1,\n10.110.0.12,\n89.111.53.68,\n10.110.40.11,\n78.81.245.219,\n109.229.76.101,\n192.168.77.2"; // test ,\n89.111.53.69
const paymentSuccess = "https://qa-"+casinoName.toLowerCase()+".egamings.com?message=PAYMENT_SUCCESS";
const paymentFail = "https://qa-"+casinoName.toLowerCase()+".egamings.com?message=PAYMENT_FAIL";
const wlcProxy = casinoType.toLowerCase()+casinoName.toLowerCase()+"_qa";

describe('Fundist login', () => {
    it('gotoFundistPayments', () => {
      cy.visit('https://qa.fundist.org/ru/Nets')
      cy.wait(1000)
      cy.get('#Login')
        .type('vshlemin')
        .should('have.value', 'vshlemin')
      cy.get('#Password')
        .type('Test123!')
        .should('have.value', 'Test123!')
      cy.get('#LoginBtn').contains('Войти').click()
      cy.wait(2000)

      /*   _   _ ______ _______ _____
          | \ | |  ____|__   __/ ____|
          |  \| | |__     | | | (___
          | . ` |  __|    | |  \___ \
          | |\  | |____   | |  ____) |
          |_| \_|______|  |_| |_____/ */

      cy.get('#newNetsBlockButton').click({ force: true })
      cy.wait(2000)
      cy.get('.modal-dialog').within(() => {
        cy.get('[name=Name]').click()
          .type(casinoName)
      })

      cy.get('.select2-selection__rendered').contains(fCountry).click()
      cy.get('.select2-dropdown').within(() => {
        cy.get('.select2-search__field')
          .type('Латвия')
          .should('have.value', 'Латвия')
          .type ('{enter}')
      })
      cy.get('#curr1').check()
      cy.get('#curr2').check()
      cy.get('#curr3').check() //test curr0 qa curr3
      cy.get('#system975').check()
      cy.get('#system985').check()
      cy.get('#system987').check()
      cy.get('#system978').check()
      cy.get('#newNetsBlockSaveButton').contains('Сохранить').click()
      cy.get('#MerchAndCurrModalConfirm').contains('ОК').click()
      cy.wait(2000)

      /*    _____ _____ _______ ______  _____
           / ____|_   _|__   __|  ____|/ ____|
          | (___   | |    | |  | |__  | (___
           \___ \  | |    | |  |  __|  \___ \
           ____) |_| |_   | |  | |____ ____) |
          |_____/|_____|  |_|  |______|_____/ */

      cy.get('#SideMenu-Stalls').click()
      cy.get('#SideMenu-Stalls-Stalls').click()
      cy.wait(2000)
      cy.get('#AddStallButton').contains('Добавить сайт').click({ force: true })
      cy.wait(2000)
      cy.get('.modal-dialog').within(() => {
        cy.get('.select2-selection__rendered').contains('Сеть').click()
      })
      cy.get('.select2-dropdown').within(() => {
        cy.get('.select2-search__field')
          .type(casinoName)
          .should('have.value', casinoName)
          .type ('{enter}')
      })
      cy.get('.modal-dialog').within(() => {
        cy.get('[name=Name]').click()
          .type(casinoName)
      })
      cy.get('[name="BalanceAlert"]')
          .type(1000)
          .should('have.value', 1000)
      cy.get('.modal-dialog').within(() => {
        cy.get('.select2-selection__rendered').contains('Выберите тип казино').click()
      })
      cy.get('.select2-results__option').contains('WLC').click()
      cy.get('#curr1').check()
      cy.get('#curr2').check()
      cy.get('#curr3').check() //test curr0 qa curr3
      cy.get('#langen').check()
      cy.get('#langru').check()
      cy.get('#NewStallBlockSaveButton').contains('Сохранить').click()
      cy.wait(2000)

      /*            _____ _____
              /\   |  __ \_   _|
             /  \  | |__) || |
            / /\ \ |  ___/ | |
           / ____ \| |    _| |_
          /_/    \_\_|   |_____| */

      cy.get('#SideMenu-Api').click()
      cy.get('#SideMenu-Api-Api').click()
      cy.wait(2000)
      cy.get('#buttonAddNewApi').contains('Добавить доступ').click({ force: true })
      cy.wait(1000)
      cy.get('.modal-dialog').within(() => {
        cy.get('.select2-selection__rendered').contains('Сеть').click()
      })
      cy.get('.select2-results__option').contains(casinoName).click()
      cy.wait(1000)
      cy.get('.modal-dialog').within(() => {
        cy.get('.select2-selection__rendered').contains('Сайт').click()
      })
      cy.get('.select2-results__option').contains(casinoName).click()
      cy.get('.modal-dialog').within(() => {
        cy.get('[name="Name"]').click()
          .type(casinoName)
      })
      cy.get('.modal-dialog').within(() => {
        cy.get('.select2-selection__rendered').contains('Тип').click()
      })
      cy.get('.select2-results__option').contains('WLC').click()
      cy.get('.modal-dialog').within(() => {
        cy.get('.select2-selection__rendered').contains('Нода').click()
      })
      cy.get('.select2-results__option').contains('1').click()
      cy.get('.modal-dialog').within(() => {
        cy.get('[name="IP"]').type(ipAdress, { delay: 1 })
      })
      cy.get('#Loyalty').check()
      cy.get('#Tournaments').check()
      cy.get('#MobileGames').check()
      cy.get('.modal-dialog').within(() => {
        cy.get('[name="UrlPaymentSuccess"]').type(paymentSuccess, { delay: 1 })
      })
      cy.get('.modal-dialog').within(() => {
        cy.get('[name="UrlPaymentFail"]').type(paymentFail, { delay: 1 })
      })
      cy.get('.modal-dialog').within(() => {
        cy.get('[name="WlcProxyPath"]').type(wlcProxy, { delay: 1 })
      })
      cy.get('.modal-dialog').within(() => {
        cy.get('[name="WlcSecret"]').type(wlcSecret)
      })
      cy.get('#AllowZeroIPHash').check()
      cy.get('#ModalApiSaveButton').contains('Сохранить').click()
      cy.get('#MessageWindowCloseButton').contains('Закрыть').click()

      // LEGAL TEXTs

      cy.get('#SideMenu-Stalls').click()
      cy.get('#SideMenu-Stalls-Stalls').click()
      cy.wait(2000)

      cy.get('[name=col-Net]').contains(casinoName).parent().within(() =>{
        cy.get('[name=Actions-edit]').click()
      })
      cy.wait(2000)

      cy.get('#textFooterContent-ru').within(() => {
        cy.get('textarea').type(ruLegal, { delay: 1 })
      })
      cy.get('#textFooterContent-en').within(() => {
        cy.get('textarea').type(enLegal, { force: true, delay: 1 })
      })

      cy.get('#NewStallBlockSaveButton').contains('Сохранить').click()
      cy.get('#ConfirmSaveModalConfirm').click()

    })
  })
  // select2-IDApi-h9-container
