// firstPayments.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test

// <reference types="cypress" />

//const casinoType = "tk"; // WLC
import * as data from '../creds.json';

const casinoName = data.casinoName;
const ruLegal = casinoName+".com is operated by SG International N.V., which is registered in Heelsumstraat 51 E-Commerce Park, Curacao, registration No. 137028 and having a gaming licence No.8048/JAZZ2015-035 and all rights to operate the gaming software. SG International LP, with its registered address Suite 1 4 Queen Street, Edinburgh, United Kingdom, EH2 1JE, registration No. SL023302, is providing payment services as an agent according to the licence agreement concluded between SG International LP and SG International N.V."
const enLegal = casinoName+".com является зарегистрированной торговой маркой SG International N.V. по адресу Хельзумштрат 51, Кюрасао и регистрационным номером 137028. Игорная деятельность организована в соответствии с правилами игорной лицензии. Номер игорной лицензии No.8048/JAZZ2015-035. Все права на эксплуатацию программного обеспечения защищены. В соответствии с этим лицензионным соглашением, посредником (исполнителем) в предоставлении платежных услуг для SG International N.V. (заказчика) выступает SG International LP, Сьют 1, Улица Квинз 4,Эдинбург, EH21JE, Шотландия, Великобритания, рег.номер SL023302. Заказчик SG International N.V. принимает эти условия"

describe('Fundist login', () => {
    it('gotoFundistSites', () => {
      cy.visit('https://qa.fundist.org/ru/Stalls')
      cy.wait(1000)
      cy.get('#Login')
        .type('vshlemin')
        .should('have.value', 'vshlemin')
      cy.get('#Password')
        .type('Test123!')
        .should('have.value', 'Test123!')
      cy.get('#LoginBtn').contains('Войти').click()
      cy.wait(2000)


      cy.get('[name=col-Net]').contains(casinoName).parent().within(() =>{
        cy.get('[name=Actions-edit]').click()
      })
      cy.wait(2000)

      cy.get('#textFooterContent-ru').within(() => {
        cy.get('textarea').type(ruLegal, { delay: 1 })
      })

      cy.get('#textFooterContent-en').within(() => {
        cy.get('textarea').type(enLegal, { force: true, delay: 1 })
      })

      cy.get('#NewStallBlockSaveButton').contains('Сохранить').click()

      cy.get('#ConfirmSaveModalConfirm').click()

      // cy.get('#AddStallButton').contains('Добавить сайт').click({ force: true })
      // cy.wait(2000)

      // cy.get('.modal-dialog').within(() => {
      //   cy.get('.select2-selection__rendered').contains('Сеть').click()
      // })

      // cy.get('.select2-dropdown').within(() => {
      //   cy.get('.select2-search__field')
      //     .type(casinoName)
      //     .should('have.value', casinoName)
      //     .type ('{enter}')
      // })

      // cy.get('.modal-dialog').within(() => {
      //   cy.get('[name=Name]').click()
      //     .type(casinoName)
      // })

      // cy.get('[name="BalanceAlert"]')
      //     .type(1000)
      //     .should('have.value', 1000)

      // cy.get('.modal-dialog').within(() => {
      //   cy.get('.select2-selection__rendered').contains('Выберите тип казино').click()
      // })
      // cy.get('.select2-results__option').contains('WLC').click()

      // cy.get('#curr1').check()
      // cy.get('#curr2').check()
      // cy.get('#curr3').check() //test curr0 qa curr3

      // cy.get('#langen').check()
      // cy.get('#langru').check()

      // cy.get('#NewStallBlockSaveButton').contains('Сохранить').click()

    })
  })
