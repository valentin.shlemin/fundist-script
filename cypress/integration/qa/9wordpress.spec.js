// firstPayments.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test

// <reference types="cypress" />

import 'cypress-file-upload';

import * as data from '../creds.json';

const casinoName = data.casinoName.toLowerCase();
const wpUser = 'adminxa';
const wpPass = 'A2KMnL76f)4!pwE)aY';

const fixtureFile = 'wpall.xml';


describe('Fundist login', () => {
    it('gotoFundistPayments', () => {
      cy.visit('https://qa-'+casinoName+'.egamings.com/content/wp/wp-admin')
      cy.wait(1000)
      cy.get('#user_login')
        .type(wpUser)
        .should('have.value', wpUser)
      cy.get('#user_pass')
        .type(wpPass)
        .should('have.value', wpPass)
      cy.get('#wp-submit').click()
      cy.wait(1000)

      // PLUGINS

      cy.get('.wp-menu-name').contains('Plugins').click()

      cy.get('[data-slug=auto-pages]').within(() => {
        cy.get('.activate').click()
      })
      cy.get('[data-slug=default-pages]').within(() => {
        cy.get('.activate').click()
      })
      cy.get('[data-slug=qtranslate-xt]').within(() => {
        cy.get('.activate').click()
      })

      // LANGUAGE

      cy.get('.wp-menu-name').contains('Settings').click()
      cy.wait(500)
      cy.get('a').contains('Permalinks').click()
      cy.wait(1000)

      cy.get('label').contains(' Post name').click()
      cy.get('#submit').click()

      cy.get('a').contains('Languages').click()
      cy.wait(1000)

      cy.get('label').contains('Use Query Mode').click()
      cy.get('#tab-general').within(() => {
        cy.get('[name=submit]').click()
      })

      cy.get('.nav-tab-wrapper').within(() => {
        cy.get('a').contains('Languages').click()
      })
      cy.get('#the-list').within(() => {
        cy.get('a').contains('Disable').click()
      })
      cy.wait(5000)
      cy.get('td').contains('Русский').parent().within(() => {
        cy.get('a').contains('Enable').click()
      })
      cy.wait(5000)

      // DEFAULT PAGES

      // cy.get('.wp-menu-name').contains('Default pages').click()

      // IMPORT

      cy.get('.wp-menu-name').contains('Tools').click()
      cy.wait(500)
      cy.get('a').contains('Import').click()
      cy.wait(1000)
      cy.get('a').contains('Run Importer').click()
      cy.get('#upload').attachFile(fixtureFile);
      cy.get('#submit').click()
      cy.get('select').select('adminxa')
      cy.get('#import-attachments').check()
      cy.get('[type=submit]').click()

    })
  })
  // select2-IDApi-h9-container
