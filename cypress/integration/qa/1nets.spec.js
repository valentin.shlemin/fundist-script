// firstPayments.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test

// <reference types="cypress" />

import * as data from '../creds.json';

const casinoName = data.casinoName;

describe('Fundist login', () => {
    it('gotoFundistPayments', () => {
      cy.visit('https://qa.fundist.org/ru/Nets')
      cy.wait(1000)
      cy.get('#Login')
        .type('vshlemin')
        .should('have.value', 'vshlemin')
      cy.get('#Password')
        .type('Test123!')
        .should('have.value', 'Test123!')
      cy.get('#LoginBtn').contains('Войти').click()
      cy.wait(2000)

      cy.get('#newNetsBlockButton').click({ force: true })
      cy.wait(2000)
      
      cy.get('.modal-dialog').within(() => {
        cy.get('[name=Name]').click()
          .type(casinoName)
      })
      //test - Австралия qa - softgamings
      cy.get('.select2-selection__rendered').contains('softgamings').click() 
        
      
      cy.get('.select2-dropdown').within(() => {
        cy.get('.select2-search__field')
          .type('Латвия')
          .should('have.value', 'Латвия')
          .type ('{enter}')
      })

      cy.get('#curr1').check() 
      cy.get('#curr2').check()
      cy.get('#curr3').check() //test curr0 qa curr3
      
      cy.get('#system975').check()
      cy.get('#system985').check()
      cy.get('#system987').check()
      cy.get('#system978').check()

      cy.get('#newNetsBlockSaveButton').contains('Сохранить').click()  

      cy.get('#MerchAndCurrModalConfirm').contains('ОК').click() 
      
    })
  })
  // select2-IDApi-h9-container
